const HTTP_AUTH_STRATEGY = {
  BASIC: 'BASIC',
  API_KEY: 'API_KEY',
  NO_AUTH: 'NO_AUTH'
}

module.exports = {
  HTTP_AUTH_STRATEGY
}
